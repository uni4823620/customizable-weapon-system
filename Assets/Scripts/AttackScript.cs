using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private Transform attackPoint;
    [SerializeField] private LayerMask enemyLayer;

    [Header("Damage Variables")]
    [SerializeField] private float physDamage;
    [SerializeField] private float magiDamage;
    [SerializeField] private float fireDamage;

    [Header("Player Stats")]
    [SerializeField] private int str;
    [SerializeField] private int dex;
    [SerializeField] private int magi;

    private Movement moveScript;
    private Animator anim;
    private Rigidbody2D rb;


    [SerializeField] private List<WeaponRaw> weapons = new List<WeaponRaw>();
    private int _currentWeapon = (int)CurrentWeapon.sword;

    private void Start()
    {
        //Example Player Statistics
        str = 60;
        dex = 30;
        magi = 40;


        moveScript = GetComponent<Movement>();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        
        //Creating new weapons
        weapons.Add(new WeaponRaw(150f, 0f, 40f, 1, (int)WeaponScale.C,(int)WeaponScale.E,(int)WeaponScale.F));//sword
        weapons.Add(new WeaponRaw(20f, 100f, 75f, 1, (int)WeaponScale.E, (int)WeaponScale.A, (int)WeaponScale.B));//staff
    }

    void Update()
    {
        //Attack logic
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Changing player's state and starting the attack animations
            moveScript.currentPlayerState = (int)PlayerState.Attacking;
            anim.SetBool("IsAttacking", true);
            rb.velocity = new Vector2(0f,0f) ;

            //Raycast to check if enemies are in range
            RaycastHit2D[] hits = Physics2D.CircleCastAll(attackPoint.position, 0.5f, Vector2.zero, 0f, enemyLayer);

            //Loop that goes through every hit enemy and damaging them
            foreach (RaycastHit2D hit in hits)
            {
                DamageCalculation();
                DamageEnemy(hit.collider.gameObject, physDamage, magiDamage, fireDamage);
            }
        }
    }

    private void AttackEnd()
    {
        //Setting animation to idle and changing player's state
        anim.SetBool("IsAttacking", false);
        moveScript.currentPlayerState = (int)PlayerState.Idle;
    }

    //Subtracting damage from enemies' health
    private void DamageEnemy(GameObject enemy, float physicalDealt, float magicalDealt, float fireDealt)
    {
        EnemyBehaviour enem = enemy.GetComponent<EnemyBehaviour>();
        enem.damageTaken = (physicalDealt * enem._physArmor) + (magicalDealt * enem._magiArmor) + (fireDealt * enem._fireArmor);
    }

    //Calculating Damage based on a formula
    private void DamageCalculation()
    {
        //physical DMG calculation
        physDamage = weapons[_currentWeapon].basePhysDmg + (((str/weapons[_currentWeapon].strScaling)* weapons[_currentWeapon].weaponLevel) +
            ((dex / weapons[_currentWeapon].dexScaling) * weapons[_currentWeapon].weaponLevel) +
            ((magi / weapons[_currentWeapon].magiScaling) * weapons[_currentWeapon].weaponLevel));
        //fire DMG calculation
        fireDamage = weapons[_currentWeapon].baseFireDmg + (((str / weapons[_currentWeapon].strScaling) * weapons[_currentWeapon].weaponLevel) +
            ((dex / weapons[_currentWeapon].dexScaling) * weapons[_currentWeapon].weaponLevel) +
            ((magi / weapons[_currentWeapon].magiScaling) * weapons[_currentWeapon].weaponLevel));
        //magi DMG calculation
        magiDamage = weapons[_currentWeapon].baseMagiDmg + (((str / weapons[_currentWeapon].strScaling) * weapons[_currentWeapon].weaponLevel) +
            ((dex / weapons[_currentWeapon].dexScaling) * weapons[_currentWeapon].weaponLevel) +
            ((magi / weapons[_currentWeapon].magiScaling) * weapons[_currentWeapon].weaponLevel));
    }
}

public enum WeaponScale
{
    A = 2,
    B,
    C,
    D,
    E,
    F
}

public enum CurrentWeapon
{ 
    sword,
    staff
}