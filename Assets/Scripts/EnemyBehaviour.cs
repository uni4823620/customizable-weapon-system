using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public float damageTaken = 0f;
    public float HP = 10;

    //100% damage reduction is 0f while no damage reduction is 1f
    public float _physArmor = 1f;
    public float _magiArmor = 0.5f;
    public float _fireArmor = 0.2f;

    private void Update()
    {
        if(damageTaken > 0f)
        {
            HP = HP - damageTaken;
            damageTaken = 0f;
        }
        
        if(HP <= 0f)
        {
            Destroy(this.gameObject);
        }
    }
}
