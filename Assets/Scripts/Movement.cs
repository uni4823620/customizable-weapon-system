using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float moveX;
    private Rigidbody2D rb;
    [SerializeField] private float speed;
    public int currentPlayerState = (int)PlayerState.Idle;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        transform.rotation = new Quaternion(0f, 180f, 0f, 0f);
    }

    void Update()
    {
        if(currentPlayerState == (int)PlayerState.Idle)
        {
            MoveLogic();
        }
        else
        {
        }
    }

    private void MoveLogic()
    {
        //Getting the horizontal axis
        moveX = Input.GetAxisRaw("Horizontal");

        //Moving the player
        rb.velocity = new Vector2(moveX * speed, rb.velocity.y);

        //Flipping player
        if (moveX < 0)
        {
            transform.rotation = new Quaternion(0f, 180f, 0f, 0f);
        }
        else if (moveX > 0)
        {
            transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        }
    }
}

public enum PlayerState
{
    Idle = 0,
    Attacking
}
