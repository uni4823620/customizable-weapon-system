using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponRaw
{
    public float basePhysDmg;
    public float baseMagiDmg;
    public float baseFireDmg;

    public int weaponLevel;

    public float strScaling;
    public float dexScaling;
    public float magiScaling;

    public WeaponRaw(float p_physDmg, float p_magiDmg, float p_fireDmg, int p_weaponLvl, float p_strScaling, float p_dexScaling, float p_magiScaling)
    {
        basePhysDmg = p_physDmg; 
        baseMagiDmg = p_magiDmg;
        baseFireDmg = p_fireDmg;
        weaponLevel = p_weaponLvl;
        strScaling = p_strScaling;
        dexScaling = p_dexScaling;
        magiScaling = p_magiScaling;
    }
}
